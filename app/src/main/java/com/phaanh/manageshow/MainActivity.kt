package com.phaanh.manageshow

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.phaanh.manageshow.musican.ManageMusicanActivity
import com.phaanh.manageshow.show.ManageShowActivity
import com.phaanh.manageshow.singer.ManageSingerActivity
import com.phaanh.manageshow.song.ManageSongActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvBieuDien.setOnClickListener {
            startActivity(Intent(this, ManageShowActivity::class.java))
        }

        tvCaSi.setOnClickListener {
            startActivity(Intent(this, ManageSingerActivity::class.java))
        }

        tvBaiHat.setOnClickListener {
            startActivity(Intent(this, ManageSongActivity::class.java))
        }

        tvNhacSi.setOnClickListener {
            startActivity(Intent(this, ManageMusicanActivity::class.java))
        }
    }
}
package com.phaanh.manageshow

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btDangNhap.setOnClickListener {
            if (etSUsername.text.toString().trim() == "admin" && etPassword.text.toString()
                    .trim() == "admin"
            ) {
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                Toast.makeText(this, "Kiem tra lai username va password!", Toast.LENGTH_LONG).show()
            }
        }

    }
}
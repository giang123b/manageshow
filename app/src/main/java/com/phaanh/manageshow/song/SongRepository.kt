package com.phaanh.manageshow.song

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class SongRepository(private val songDao: SongDao) {

    val allSongs: LiveData<List<Song>> = songDao.getAllSongs()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertSong(song:Song) {
        songDao.insertSong(song)
    }

    suspend fun updateSong(song:Song) {
        songDao.updateSong(
            song.id, song.name
        )
    }

    suspend fun deleteSong(song:Song) {
        songDao.deleteSong(song.id)
    }
}

package com.phaanh.manageshow.song

import android.app.Application
import android.content.Context
import android.media.MediaPlayer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.phaanh.manageshow.R
import com.phaanh.manageshow.singer.Singer
import com.phaanh.manageshow.singer.SingerViewModel
import kotlinx.android.synthetic.main.item_musican.view.*
import kotlinx.android.synthetic.main.item_singer.view.*
import kotlinx.android.synthetic.main.item_song.view.*

class SongAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val mListSong = ArrayList<Song>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SongViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_song,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = mListSong.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SongViewHolder).bindData(mListSong[position])
    }

    fun submitList(datas: List<Song>) {
        mListSong.clear()
        mListSong.addAll(datas)
        notifyDataSetChanged()
    }
}


class SongViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(song: Song) {
        itemView.run {
            tvMaBaiHat.setText(song.id)
            tvTenBaiHat.setText(song.name)

            val songViewModel = SongViewModel(context.applicationContext as Application)

            imXoaBaiHat.setOnClickListener {
                songViewModel.deleteSong(song)
            }

            imSuaBaiHat.setOnClickListener {
                val new = Song(tvMaBaiHat.text.toString(), tvTenBaiHat.text.toString())
                songViewModel.updateSong(new)
            }
        }
    }
}
package com.phaanh.manageshow.song

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.phaanh.manageshow.R
import kotlinx.android.synthetic.main.activity_manage_song.*

class ManageSongActivity : AppCompatActivity() {

    val mListSong = ArrayList<Song>()
    val mSongAdapter = SongAdapter()
    private lateinit var songViewModel: SongViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_song)


        rvBaiHat.adapter = mSongAdapter
        rvBaiHat.layoutManager = LinearLayoutManager(this)
//        mSongAdapter.submitList(mListSong)


        songViewModel = ViewModelProvider(this).get(SongViewModel::class.java)

        songViewModel.allSongs.observe(this, Observer { a ->
            a?.let { mSongAdapter.submitList(it) }
        })

        imThemBaiHat.setOnClickListener {

            val mDialogView =
                LayoutInflater.from(this).inflate(R.layout.input_song, null)
            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setTitle("Nhập thông show:")
            val mAlertDialog = mBuilder.show()

            val buttonSave = mDialogView.findViewById<Button>(R.id.btLuuSong)
            val buttonCancel = mDialogView.findViewById<Button>(R.id.btHuySong)

            buttonCancel.setOnClickListener {
                mAlertDialog.dismiss()
            }

            buttonSave.setOnClickListener {
                val id : TextView =  mDialogView.findViewById(R.id.etMaBaiHat)
                val name : TextView =  mDialogView.findViewById(R.id.etTenBaiHat)
                val new = Song(id.text.toString(), name.text.toString())
                songViewModel.insertSong(new)
                mAlertDialog.dismiss()
            }
        }

    }
}
package com.phaanh.manageshow.song

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class SongViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var repository: SongRepository

    lateinit var allSongs: LiveData<List<Song>>

    init {
        val showDao = SongRoomDatabase.getDatabase(application, viewModelScope).musicDao()
        repository = SongRepository(showDao)
        allSongs = repository.allSongs
    }

    fun getAll() = repository.allSongs.value

    fun insertSong(song: Song) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertSong(song)
    }

    fun updateSong(song: Song) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateSong(song)
    }

    fun deleteSong(song: Song) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteSong(song)
    }
}

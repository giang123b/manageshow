package com.phaanh.manageshow.song

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


@Database(entities = [Song::class], version = 1)
abstract class SongRoomDatabase : RoomDatabase() {

    abstract fun musicDao(): SongDao

    companion object {
        @Volatile
        private var INSTANCE: SongRoomDatabase? = null

        fun getDatabase(
                context: Context,
                scope: CoroutineScope
        ): SongRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        SongRoomDatabase::class.java,
                        "song_database"
                )
                        .fallbackToDestructiveMigration()
                        .addCallback(MusicanDatabaseCallback(scope))
                        .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }

        private class MusicanDatabaseCallback(
                private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {

            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateDatabase(database.musicDao())
                    }
                }
            }
        }

        fun populateDatabase(songDao: SongDao) {
            var song = (Song("BH01", "Kẻ cắp gặp bà già"))
            songDao.insertSong(song)
            song = (Song("BH02", "Buồn làm chi em ơi"))
            songDao.insertSong(song)
            song = (Song("BH03", "Có chắc yêu là đây"))
            songDao.insertSong(song)
            song = (Song("BH04", "Để mị nói cho mà nghe"))
            songDao.insertSong(song)
            song = (Song("BH05", "Duyên âm"))
            songDao.insertSong(song)
            song = (Song("BH06", "Gác lại lo âu"))
            songDao.insertSong(song)

        }
    }

}

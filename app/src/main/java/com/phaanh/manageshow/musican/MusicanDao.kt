/*
 * Copyright (C) 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.phaanh.manageshow.musican

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import com.phaanh.manageshow.musican.Musican


@Dao
interface MusicanDao {

    @Query("SELECT * from musican_table ORDER BY name ASC")
    fun getAllMusicans(): LiveData<List<Musican>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertMusican(musican: Musican)

    @Query("UPDATE musican_table SET id =:id, name =:name WHERE id = :id")
    fun updateMusican(id: String, name: String): Int

    @Query("DELETE FROM musican_table WHERE id =:id")
    fun deleteMusican(id: String)
}

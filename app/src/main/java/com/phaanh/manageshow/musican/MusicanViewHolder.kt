package com.phaanh.manageshow.musican

import android.app.AlertDialog
import android.app.Application
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.phaanh.manageshow.R
import kotlinx.android.synthetic.main.item_musican.view.*


class MusicanAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val mListMusican = ArrayList<Musican>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SingerViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_musican,
                parent,
                false
            ),
            context
        )
    }

    override fun getItemCount(): Int = mListMusican.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SingerViewHolder).bindData(mListMusican[position])
    }

    fun submitList(datas: List<Musican>) {
        mListMusican.clear()
        mListMusican.addAll(datas)
        notifyDataSetChanged()
    }

}


class SingerViewHolder(itemView: View, context:Context) : RecyclerView.ViewHolder(itemView) {

    fun bindData(musican: Musican) {
        itemView.run {
            tvMaNhacSi.setText(musican.id)
            tvTenNhacSi.setText(musican.name)

            val musicanViewModel = MusicanViewModel(context.applicationContext as Application)

            imXoaNhacSi.setOnClickListener {
                musicanViewModel.deleteMusican(musican)
            }

            imSuaNhacSi.setOnClickListener {
                val newMusican = Musican(tvMaNhacSi.text.toString(), tvTenNhacSi.text.toString())
                musicanViewModel.updateMusican(newMusican)
            }
        }
    }
}

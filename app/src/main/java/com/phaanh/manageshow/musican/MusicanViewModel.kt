package com.phaanh.manageshow.musican

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class MusicanViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var repository: MusicanRepository

    lateinit var allMusicans: LiveData<List<Musican>>

    init {
        val musicanDao = MusicRoomDatabase.getDatabase(application, viewModelScope).musicDao()
        repository = MusicanRepository(musicanDao)
        allMusicans = repository.allMusicans
    }

    fun getAllMusic() = repository.allMusicans.value

    fun insertMusican(musican: Musican) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertMusican(musican)
    }

    fun updateMusican(musican: Musican) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateMusican(musican)
    }

    fun deleteMusican(musican: Musican) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteMusican(musican)
    }
}

package com.phaanh.manageshow.musican

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


@Database(entities = [Musican::class], version = 1)
abstract class MusicRoomDatabase : RoomDatabase() {

    abstract fun musicDao(): MusicanDao

    companion object {
        @Volatile
        private var INSTANCE: MusicRoomDatabase? = null

        fun getDatabase(
                context: Context,
                scope: CoroutineScope
        ): MusicRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        MusicRoomDatabase::class.java,
                        "musican_database"
                )
                        .fallbackToDestructiveMigration()
                        .addCallback(MusicanDatabaseCallback(scope))
                        .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }

        private class MusicanDatabaseCallback(
                private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {

            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateDatabase(database.musicDao())
                    }
                }
            }
        }

        fun populateDatabase(musicanDao: MusicanDao) {
            var musican = Musican("NS01", "Son Tung MTP")
            musicanDao.insertMusican(musican)
            musican = Musican("NS02", "Khac Hung")
            musicanDao.insertMusican(musican)
            musican = Musican("NS03", "Ho Hoai Anh")
            musicanDao.insertMusican(musican)
            musican = Musican("NS04", "Khac Viet")
            musicanDao.insertMusican(musican)
            musican = Musican("NS05", "Tien Cookie")
            musicanDao.insertMusican(musican)
        }
    }

}

package com.phaanh.manageshow.musican

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.phaanh.manageshow.R
import kotlinx.android.synthetic.main.activity_manage_musican.*
import kotlinx.android.synthetic.main.activity_manage_show.*


class ManageMusicanActivity : AppCompatActivity() {

    val mMusicanAdapter = MusicanAdapter(this)
    private lateinit var musicanViewModel: MusicanViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_musican)


        rvNhacSi.adapter = mMusicanAdapter
        rvNhacSi.layoutManager = LinearLayoutManager(this)

        musicanViewModel = ViewModelProvider(this).get(MusicanViewModel::class.java)

        musicanViewModel.allMusicans.observe(this, Observer { a ->

            a?.let { mMusicanAdapter.submitList(it) }
        })

        imThemNhacSi.setOnClickListener {

            val mDialogView =
                LayoutInflater.from(this).inflate(R.layout.input_musican, null)
            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setTitle("Nhập thông show:")
            val mAlertDialog = mBuilder.show()

            val buttonSave = mDialogView.findViewById<Button>(R.id.btLuuMusican)
            val buttonCancel = mDialogView.findViewById<Button>(R.id.btHuyMusican)

            buttonCancel.setOnClickListener {
                mAlertDialog.dismiss()
            }

            buttonSave.setOnClickListener {
                val id :TextView=  mDialogView.findViewById(R.id.etMaNhacSi)
                val name :TextView=  mDialogView.findViewById(R.id.etTenNhacSi)
                val musican = Musican(id.text.toString(), name.text.toString())
                musicanViewModel.insertMusican(musican)
                mAlertDialog.dismiss()
            }
        }
    }
}
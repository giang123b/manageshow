package com.phaanh.manageshow.musican

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class MusicanRepository(private val musicanDao: MusicanDao) {

    val allMusicans: LiveData<List<Musican>> = musicanDao.getAllMusicans()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertMusican(musican: Musican) {
        musicanDao.insertMusican(musican)
    }

    suspend fun updateMusican(musican: Musican) {
        musicanDao.updateMusican(musican.id, musican.name)
    }

    suspend fun deleteMusican(musican: Musican) {
        musicanDao.deleteMusican(musican.id)
    }
}

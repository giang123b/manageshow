package com.phaanh.manageshow.singer

import android.app.Application
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.phaanh.manageshow.R
import com.phaanh.manageshow.musican.Musican
import kotlinx.android.synthetic.main.item_musican.view.*
import kotlinx.android.synthetic.main.item_singer.view.*

class SingerAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val mListSinger = ArrayList<Singer>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SingerViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_singer,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = mListSinger.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SingerViewHolder).bindData(mListSinger[position])


    }

    fun submitList(datas: List<Singer>) {
        mListSinger.clear()
        mListSinger.addAll(datas)
        notifyDataSetChanged()
    }

}


class SingerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


    fun bindData(singer: Singer) {
        itemView.run {
            tvMaCaSi.setText(singer.id)
            tvTenCaSi.setText(singer.name)

            val singerViewModel = SingerViewModel(context.applicationContext as Application)

            imXoaCaSi.setOnClickListener {
                singerViewModel.deleteSinger(singer)
            }

            imSuaCaSi.setOnClickListener {
                val new = Singer(tvMaCaSi.text.toString(), tvTenCaSi.text.toString())
                singerViewModel.updateSinger(new)
            }
        }
    }
}
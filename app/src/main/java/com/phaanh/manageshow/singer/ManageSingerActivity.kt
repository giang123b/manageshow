package com.phaanh.manageshow.singer

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.phaanh.manageshow.R
import com.phaanh.manageshow.musican.Musican
import com.phaanh.manageshow.show.ShowViewModel
import kotlinx.android.synthetic.main.activity_manage_show.*
import kotlinx.android.synthetic.main.activity_manage_singer.*

class ManageSingerActivity : AppCompatActivity() {

    val mListSinger = ArrayList<Singer>()
    val mSingerAdapter = SingerAdapter()
    private lateinit var singerViewModel: SingerViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_singer)

        rvCaSi.adapter = mSingerAdapter
        rvCaSi.layoutManager = LinearLayoutManager(this)
//        mSingerAdapter.submitList(mListSinger)


        singerViewModel = ViewModelProvider(this).get(SingerViewModel::class.java)

        singerViewModel.allSinger.observe(this, Observer { a ->
            a?.let { mSingerAdapter.submitList(it) }
        })


        imThemCaSi.setOnClickListener {

            val mDialogView =
                LayoutInflater.from(this).inflate(R.layout.input_singer, null)
            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setTitle("Nhập thông show:")
            val mAlertDialog = mBuilder.show()

            val buttonSave = mDialogView.findViewById<Button>(R.id.btLuuSinger)
            val buttonCancel = mDialogView.findViewById<Button>(R.id.btHuySinger)

            buttonCancel.setOnClickListener {
                mAlertDialog.dismiss()
            }

            buttonSave.setOnClickListener {
                val id : TextView =  mDialogView.findViewById(R.id.etMaCaSi)
                val name : TextView =  mDialogView.findViewById(R.id.etTenCaSi)
                val new = Singer(id.text.toString(), name.text.toString())
                singerViewModel.insertSinger(new)
                mAlertDialog.dismiss()
            }
        }

    }
}
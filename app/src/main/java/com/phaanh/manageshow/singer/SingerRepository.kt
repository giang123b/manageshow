package com.phaanh.manageshow.singer

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class SingerRepository(private val singerDao: SingerDao) {

    val allSingers: LiveData<List<Singer>> = singerDao.getAllSingers()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertSinger(singer: Singer) {
        singerDao.insertSinger(singer)
    }

    suspend fun updateSinger(singer: Singer) {
        singerDao.updateSinger(
            singer.id, singer.name
        )
    }

    suspend fun deleteSinger(singer: Singer) {
        singerDao.deleteSinger(singer.id)
    }
}

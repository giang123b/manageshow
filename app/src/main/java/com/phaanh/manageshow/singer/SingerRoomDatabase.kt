package com.phaanh.manageshow.singer

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


@Database(entities = [Singer::class], version = 1)
abstract class SingerRoomDatabase : RoomDatabase() {

    abstract fun musicDao(): SingerDao

    companion object {
        @Volatile
        private var INSTANCE: SingerRoomDatabase? = null

        fun getDatabase(
                context: Context,
                scope: CoroutineScope
        ): SingerRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        SingerRoomDatabase::class.java,
                        "singer_database"
                )
                        .fallbackToDestructiveMigration()
                        .addCallback(MusicanDatabaseCallback(scope))
                        .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }

        private class MusicanDatabaseCallback(
                private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {

            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateDatabase(database.musicDao())
                    }
                }
            }
        }

        fun populateDatabase(singerDao: SingerDao) {
            var show = (Singer("CS01", "Son Tung MTP"))
            singerDao.insertSinger(show)
            show =(Singer("CS02", "Nam Cao"))
            singerDao.insertSinger(show)
            show =(Singer("CS03", "Hoa Minzy"))
            singerDao.insertSinger(show)
            show =(Singer("CS04", "Noo Phuoc Thinh"))
            singerDao.insertSinger(show)
            show =(Singer("CS05", "Tuan Hung"))
            singerDao.insertSinger(show)

        }
    }

}

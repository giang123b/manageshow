package com.phaanh.manageshow.singer

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class SingerViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var repository: SingerRepository

    lateinit var allSinger: LiveData<List<Singer>>

    init {
        val showDao = SingerRoomDatabase.getDatabase(application, viewModelScope).musicDao()
        repository = SingerRepository(showDao)
        allSinger = repository.allSingers
    }

    fun getAll() = repository.allSingers.value

    fun insertSinger(singer: Singer) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertSinger(singer)
    }

    fun updateSinger(singer: Singer) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateSinger(singer)
    }

    fun deleteSinger(singer: Singer) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteSinger(singer)
    }
}

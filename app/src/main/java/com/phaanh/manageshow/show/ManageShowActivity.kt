package com.phaanh.manageshow.show

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.phaanh.manageshow.R
import com.phaanh.manageshow.musican.Musican
import kotlinx.android.synthetic.main.activity_manage_show.*

class ManageShowActivity : AppCompatActivity() {

    val mListShow = ArrayList<Show>()
    val mShowAdapter = ShowAdapter()

    private lateinit var showViewModel: ShowViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_show)

        rvBieuDien.adapter = mShowAdapter
        rvBieuDien.layoutManager = LinearLayoutManager(this)

        showViewModel = ViewModelProvider(this).get(ShowViewModel::class.java)

        showViewModel.allShows.observe(this, Observer { a ->
            a?.let { mShowAdapter.submitList(it) }
        })


        imThemBieuDien.setOnClickListener {

            val mDialogView =
                LayoutInflater.from(this).inflate(R.layout.input_show, null)
            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setTitle("Nhập thông show:")
            val mAlertDialog = mBuilder.show()

            val buttonSave = mDialogView.findViewById<Button>(R.id.btLuuShow)
            val buttonCancel = mDialogView.findViewById<Button>(R.id.btHuyShow)

            buttonCancel.setOnClickListener {
                mAlertDialog.dismiss()
            }

            buttonSave.setOnClickListener {
                val idBD : TextView =  mDialogView.findViewById(R.id.etMaBieuDien)
                val idCS : TextView =  mDialogView.findViewById(R.id.etMaCaSi)
                val idNS : TextView =  mDialogView.findViewById(R.id.etMaNhacSi)
                val idBH : TextView =  mDialogView.findViewById(R.id.etMaBaiHat)
                val time : TextView =  mDialogView.findViewById(R.id.etThoiGian)
                val location : TextView =  mDialogView.findViewById(R.id.etDiaDiem)
                val show = Show(idBD.text.toString(),
                    idCS.text.toString(),
                    idNS.text.toString(),
                    idBH.text.toString(),
                    time.text.toString(),
                    location.text.toString()
                )
                showViewModel.insertShow(show)
                mAlertDialog.dismiss()
            }
        }
    }
}
package com.phaanh.manageshow.show

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class ShowRepository(private val showDao: ShowDao) {

    val allMusicans: LiveData<List<Show>> = showDao.getAllShows()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertShow(show: Show) {
        showDao.insertShow(show)
    }

    suspend fun updateShow(show: Show) {
        showDao.updateShow(
            show.idShow, show.idCS, show.idNS
            , show.idBH, show.time, show.location
        )
    }

    suspend fun deleteShow(show: Show) {
        showDao.deleteShow(show.idShow)
    }
}

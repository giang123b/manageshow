package com.phaanh.manageshow.show

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ShowViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var repository: ShowRepository

    lateinit var allShows: LiveData<List<Show>>

    init {
        val showDao = ShowRoomDatabase.getDatabase(application, viewModelScope).musicDao()
        repository = ShowRepository(showDao)
        allShows = repository.allMusicans
    }

    fun getAll() = repository.allMusicans.value

    fun insertShow(show: Show) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertShow(show)
    }

    fun updateShow(show: Show) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateShow(show)
    }

    fun deleteShow(show: Show) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteShow(show)
    }
}

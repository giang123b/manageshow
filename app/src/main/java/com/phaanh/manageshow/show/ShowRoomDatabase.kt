package com.phaanh.manageshow.show

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


@Database(entities = [Show::class], version = 1)
abstract class ShowRoomDatabase : RoomDatabase() {

    abstract fun musicDao(): ShowDao

    companion object {
        @Volatile
        private var INSTANCE: ShowRoomDatabase? = null

        fun getDatabase(
                context: Context,
                scope: CoroutineScope
        ): ShowRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        ShowRoomDatabase::class.java,
                        "show_database"
                )
                        .fallbackToDestructiveMigration()
                        .addCallback(MusicanDatabaseCallback(scope))
                        .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }

        private class MusicanDatabaseCallback(
                private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {

            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateDatabase(database.musicDao())
                    }
                }
            }
        }

        fun populateDatabase(showDao: ShowDao) {
            var show = (Show("BH01","CS01", "NS01","BH01","20:00 01-08-2020","Bar 1900"))
            showDao.insertShow(show)
            show = (Show("BD02", "CS02","NS02","BH02","20:00 03-08-2020","Haniken Da Nang"))
            showDao.insertShow(show)
            show = (Show("BD03", "CS03","NS03","BH03","20:00 04-08-2020","Tiger Ha Noi"))
            showDao.insertShow(show)
            show = (Show("BD04", "CS04","NS04","BH04","20:00 05-08-2020","Ho Guom"))
            showDao.insertShow(show)
            show = (Show("BD05", "CS05","NS05","BH05","20:00 06-08-2020","Nha hat lon"))
            showDao.insertShow(show)

        }
    }

}

package com.phaanh.manageshow.show

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "show_table")
data class Show(
    @PrimaryKey @ColumnInfo(name = "idShow")
    val idShow: String,
    @ColumnInfo(name = "idCS")
    val idCS: String,
    @ColumnInfo(name = "idNS")
    val idNS: String,
    @ColumnInfo(name = "idBH")
    val idBH: String,
    @ColumnInfo(name = "time")
    val time: String,
    @ColumnInfo(name = "location")
    val location: String
) {
}
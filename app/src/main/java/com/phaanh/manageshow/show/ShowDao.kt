/*
 * Copyright (C) 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.phaanh.manageshow.show

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import com.phaanh.manageshow.musican.Musican


@Dao
interface ShowDao {

    @Query("SELECT * from show_table ORDER BY idShow ASC")
    fun getAllShows(): LiveData<List<Show>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertShow(show: Show)

    @Query("UPDATE show_table SET idShow =:idShow, idCS =:idCS, idNS =:idNS, idBH =:idBH, time =:time, location =:location WHERE idShow = :idShow")
    fun updateShow(
        idShow: String,
        idCS: String,
        idNS: String,
        idBH: String,
        time: String,
        location: String
    ): Int

    @Query("DELETE FROM show_table WHERE idShow =:idShow")
    fun deleteShow(idShow: String)
}

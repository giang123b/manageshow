package com.phaanh.manageshow.show

import android.app.Application
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.phaanh.manageshow.R
import com.phaanh.manageshow.musican.Musican
import kotlinx.android.synthetic.main.item_musican.view.*
import kotlinx.android.synthetic.main.item_show.view.*
import java.util.*
import kotlin.collections.ArrayList

class ShowAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    val mListShow = ArrayList<Show>()
    var mListShowFilter = ArrayList<Show>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SingerViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_show,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = mListShow.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SingerViewHolder).bindData(mListShow[position])


    }

    fun submitList(datas: List<Show>) {
        mListShow.clear()
        mListShow.addAll(datas)

        mListShowFilter = mListShow

        notifyDataSetChanged()
    }

}


class SingerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


    fun bindData(show: Show) {
        itemView.run {
            tvMaBieuDien.setText(show.idShow)
            tvMACaSiBD.setText(show.idCS)
            tvMaNhacSiBD.setText(show.idNS)
            tvMaBaiHatBD.setText(show.idBH)
            tvThoiGian.setText(show.time)
            tvDiaDiem.setText(show.location)

            val showViewModel = ShowViewModel(context.applicationContext as Application)

            imXoaShow.setOnClickListener {
                showViewModel.deleteShow(show)
            }

            imSuaShow.setOnClickListener {


                val new = Show(
                    tvMaBieuDien.text.toString(),
                    tvMACaSiBD.text.toString(),
                    tvMaNhacSiBD.text.toString(),
                    tvMaBaiHatBD.text.toString(),
                    tvThoiGian.text.toString(),
                    tvDiaDiem.text.toString()
                )
                showViewModel.updateShow(new)
            }

        }
    }
}